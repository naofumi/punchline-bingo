Punchline Bingo
===============

<img src="https://gitlab.com/uploads/project/avatar/437830/bingo.png" alt="Punchline Bingo logo" width="300">

Licensed under the [GPL v3 licence](https://www.gnu.org/licenses/gpl-3.0.en.html).

[Télécharger/Download](https://gitlab.com/pierreduchemin/punchline-bingo/raw/master/punchline-bingo-latest.apk)
<img src="https://gitlab.com/pierreduchemin/punchline-bingo/raw/master/resources/latest_apk_download.png" alt="Download qr code" width="300">

Description
-----------

This application allows you to play bingo alone or with friends. / Cette application vous permet de jouer au bingo seul ou entre amis.
The idea came from these bingo grids posted on social medias : / L'idée est venue de ces grilles de bingo postés sur divers réseaux sociaux :

<img src="https://gitlab.com/pierreduchemin/punchline-bingo/raw/master/resources/grid1.jpg" alt="Bingo grid 1" width="400">
<img src="https://gitlab.com/pierreduchemin/punchline-bingo/raw/master/resources/grid2.png" alt="Bingo grid 2" width="400">

Utilisation
-----------

* Copy apk file on your device
* Allow unknown sources in Android settings
* Use a file manager to install
* Enjoy!

---

* Copiez le fichier apk sur votre appareil
* Autorisez les sources inconnues dans les paramètres Android
* Utilisez un gestionnaire de fichiers pour lancer l'installation
* Profitez !
