package com.pierreduchemin.punchlinebingo;

import com.pierreduchemin.punchlinebingo.models.Game;
import com.pierreduchemin.punchlinebingo.models.Punchline;
import com.pierreduchemin.punchlinebingo.models.PunchlinesSet;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Game local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class GameUnitTest {

    @Test
    public void init_test() {
        Game g = new Game();

        assertFalse(g.hasPunchlines());
        assertFalse(g.isFinished());
        assertTrue(g.getCurrentPunchlines().isEmpty());
    }

    @Test
    public void hasPunchlines_test() {
        Game g = new Game();

        assertFalse(g.hasPunchlines());

        g.getCurrentPunchlines().add(new Punchline("MyPunchline"));

        assertTrue(g.hasPunchlines());
    }

    @Test
    public void generateRandomPunchlines_test() {
        Game g = new Game();
        ArrayList<PunchlinesSet> punchlinesSets = new ArrayList<>();
        PunchlinesSet ps = new PunchlinesSet("MyPunchlineSet");
        for (int index = 0; index < Game.PUNCHLINES_BY_GAME_NB; index++) {
            ps.addPunchline("MyPunchline" + index);
        }
        punchlinesSets.add(ps);

        assertTrue(g.generateRandomPunchlines(punchlinesSets, 0));
    }

    @Test
    public void generateRandomPunchlines_fail_test() {
        Game g = new Game();
        ArrayList<PunchlinesSet> punchlinesSets = new ArrayList<>();
        PunchlinesSet ps = new PunchlinesSet("MyPunchlineSet");
        punchlinesSets.add(ps);

        assertFalse(g.generateRandomPunchlines(punchlinesSets, 0));
    }
}