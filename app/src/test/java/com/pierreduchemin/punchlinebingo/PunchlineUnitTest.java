package com.pierreduchemin.punchlinebingo;

import com.pierreduchemin.punchlinebingo.models.Punchline;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Punchline local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class PunchlineUnitTest {

    @Test
    public void init_test() {
        String label = "MyPunchline";
        Punchline p = new Punchline(label);

        assertFalse(p.isChecked());
        assertEquals(label, p.getLabel());
    }

    @Test
    public void checking_test() {
        Punchline p = new Punchline("MyPunchline");
        p.setChecked(true);

        assertTrue(p.isChecked());
    }
}