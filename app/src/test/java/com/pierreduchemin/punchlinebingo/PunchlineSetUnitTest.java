package com.pierreduchemin.punchlinebingo;

import com.pierreduchemin.punchlinebingo.models.Game;
import com.pierreduchemin.punchlinebingo.models.Punchline;
import com.pierreduchemin.punchlinebingo.models.PunchlinesSet;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * PunchlineSet local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class PunchlineSetUnitTest {

    @Test
    public void init_test() {
        String name = "MySet";
        PunchlinesSet ps = new PunchlinesSet(name);

        assertEquals(name, ps.getName());
        assertTrue(ps.getAvailablePunchlines().isEmpty());
        assertFalse(ps.isPlayable());
    }

    @Test
    public void addPunchline_test() {
        PunchlinesSet ps = new PunchlinesSet("MySet");
        ps.addPunchline("MyPunchline");

        assertEquals(1, ps.getPunchlinesNb());
        assertFalse(ps.isPlayable());
    }

    @Test
    public void getAvailablePunchlines_test() {
        PunchlinesSet ps = new PunchlinesSet("MySet");
        ps.addPunchline("MyPunchline");

        assertEquals(1, ps.getAvailablePunchlines().size());
        assertEquals(Punchline.class, ps.getAvailablePunchlines().get(0).getClass());
    }

    @Test
    public void containPunchline_test() {
        PunchlinesSet ps = new PunchlinesSet("MySet");
        String test = "MyPunchline";
        ps.addPunchline(test);

        assertTrue(ps.containPunchline(test));
        assertFalse(ps.containPunchline("NotExistingPunchline"));
    }

    @Test
    public void containPunchline_null_test() {
        PunchlinesSet ps = new PunchlinesSet("MySet");

        assertFalse(ps.containPunchline(null));
    }

    @Test
    public void getName_test() {
        String name = "MySet";
        PunchlinesSet ps = new PunchlinesSet(name);

        assertEquals(name, ps.getName());
    }

    @Test
    public void getPunchlinesNb_test() {
        PunchlinesSet ps = new PunchlinesSet("MySet");
        ps.addPunchline("MyPunchline");
        ps.addPunchline("MyPunchline");
        ps.addPunchline("MyPunchline");

        assertEquals(3, ps.getPunchlinesNb());
    }

    @Test
    public void isPlayable_test() {
        PunchlinesSet ps = new PunchlinesSet("MySet");
        for (int index = 0; index < Game.PUNCHLINES_BY_GAME_NB - 1; index++) {
            ps.addPunchline("MyPunchline");
        }

        assertFalse(ps.isPlayable());

        ps.addPunchline("MyPunchline");

        assertTrue(ps.isPlayable());
    }

    @Test
    public void removePunchline_test() {
        PunchlinesSet ps = new PunchlinesSet("MySet");
        ps.addPunchline("MyPunchline");

        assertEquals(1, ps.getPunchlinesNb());

        ps.removePunchline(ps.getAvailablePunchlines().get(0));

        assertTrue(ps.getPunchlinesNb() == 0);
    }
}