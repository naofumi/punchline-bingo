package com.pierreduchemin.punchlinebingo.activities;


import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.pierreduchemin.punchlinebingo.R;
import com.pierreduchemin.punchlinebingo.models.Game;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.allOf;

@RunWith(AndroidJUnit4.class)
public class GameActivity_victory_Test {

    @Rule
    public ActivityTestRule<MainActivity> mainActivityActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void mainActivityTest() {
        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.btnGame), isDisplayed()));
        appCompatButton.perform(click());

        onView(withId(R.id.rvPunchlinesSetSelector))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));

        for (int index = 0; index < Game.PUNCHLINES_BY_GAME_NB; index++) {
            onView(withId(R.id.rvGame))
                    .perform(RecyclerViewActions.actionOnItemAtPosition(index, click()));

            if (index == Game.PUNCHLINES_BY_GAME_NB - 1) {
                onView(withId(R.id.llVictory))
                        .check(matches(isDisplayed()));
            }
        }
    }
}
