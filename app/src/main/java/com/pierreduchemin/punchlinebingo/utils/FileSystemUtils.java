/*
 * Copyright © 2017.
 * This file is part of Punchline Bingo.
 *
 * Punchline Bingo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Punchline Bingo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Punchline Bingo.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.pierreduchemin.punchlinebingo.utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.RawRes;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.pierreduchemin.punchlinebingo.models.PunchlinesSet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

public class FileSystemUtils {

    public static List<PunchlinesSet> getPunchlinesFromJson(@NonNull Context context, @RawRes int jsonRawResId) throws IOException {
        InputStream jsonInputStream = context.getResources().openRawResource(jsonRawResId);

        String json = readFile(jsonInputStream);

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(PunchlinesSet.class, PunchlinesSetDeserialiser.get());
        return gsonBuilder.create().fromJson(json, new TypeToken<List<PunchlinesSet>>() {
        }.getType());
    }

    private static String readFile(@NonNull InputStream inputStream) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }
        bufferedReader.close();

        return stringBuilder.toString();
    }
}