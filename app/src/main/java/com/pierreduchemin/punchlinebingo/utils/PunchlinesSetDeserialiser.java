/*
 * Copyright © 2017.
 * This file is part of Punchline Bingo.
 *
 * Punchline Bingo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Punchline Bingo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Punchline Bingo.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.pierreduchemin.punchlinebingo.utils;

import android.support.annotation.NonNull;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.pierreduchemin.punchlinebingo.models.PunchlinesSet;

import java.lang.reflect.Type;


class PunchlinesSetDeserialiser implements JsonDeserializer<PunchlinesSet> {

    private static PunchlinesSetDeserialiser instance;

    private PunchlinesSetDeserialiser() {
    }

    @NonNull
    static PunchlinesSetDeserialiser get() {
        if (instance == null) {
            instance = new PunchlinesSetDeserialiser();
        }
        return instance;
    }

    @Override
    @NonNull
    public PunchlinesSet deserialize(@NonNull JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) {

        JsonObject jsonObjectPunchlinesSet = jsonElement.getAsJsonObject();

        String name = jsonObjectPunchlinesSet.get("name").getAsString();

        PunchlinesSet punchlinesSet = new PunchlinesSet(name);
        JsonArray jsonPunchlines = jsonObjectPunchlinesSet.get("punchlines").getAsJsonArray();

        for (JsonElement punchline : jsonPunchlines) {
            punchlinesSet.addPunchline(punchline.getAsString());
        }

        return punchlinesSet;
    }
}
