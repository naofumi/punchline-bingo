/*
 * Copyright © 2017.
 * This file is part of Punchline Bingo.
 *
 * Punchline Bingo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Punchline Bingo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Punchline Bingo.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.pierreduchemin.punchlinebingo.models;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

public class PunchlinesSet {

    @NonNull
    private String name;
    @NonNull
    private List<Punchline> availablePunchlines;

    public PunchlinesSet(@NonNull String name) {
        this.name = name;
        this.availablePunchlines = new ArrayList<>();
    }

    public void addPunchline(@NonNull String content) {
        Punchline punchline = new Punchline(content);
        availablePunchlines.add(punchline);
    }

    @NonNull
    public List<Punchline> getAvailablePunchlines() {
        return availablePunchlines;
    }

    public boolean containPunchline(@Nullable String sentence) {
        for (Punchline punchline : availablePunchlines) {
            if (punchline.getLabel().equals(sentence)) {
                return true;
            }
        }
        return false;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public int getPunchlinesNb() {
        return availablePunchlines.size();
    }

    public boolean isPlayable() {
        return availablePunchlines.size() >= 12;
    }

    public boolean removePunchline(@NonNull Punchline punchlineToRemove) {
        return availablePunchlines.remove(punchlineToRemove);
    }
}
