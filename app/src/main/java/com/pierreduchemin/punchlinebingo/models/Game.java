/*
 * Copyright © 2017.
 * This file is part of Punchline Bingo.
 *
 * Punchline Bingo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Punchline Bingo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Punchline Bingo.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.pierreduchemin.punchlinebingo.models;

import android.support.annotation.IntRange;
import android.support.annotation.NonNull;

import com.annimon.stream.Stream;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Game {

    public static final int PUNCHLINES_BY_GAME_NB = 12;

    @NonNull
    private List<Punchline> currentPunchlines;

    public Game() {
        currentPunchlines = new ArrayList<>();
    }

    public boolean hasPunchlines() {
        return !currentPunchlines.isEmpty();
    }

    public boolean generateRandomPunchlines(@NonNull List<PunchlinesSet> punchlinesSets, @IntRange(from = 0) int setId) {
        currentPunchlines.clear();
        try {
            PunchlinesSet punchlinesSet = punchlinesSets.get(setId);
            currentPunchlines.addAll(punchlinesSet.getAvailablePunchlines());
            Collections.shuffle(currentPunchlines);
            currentPunchlines = currentPunchlines.subList(0, PUNCHLINES_BY_GAME_NB);
            resetPunchlines();
        } catch (IndexOutOfBoundsException e) {
            return false;
        }
        return true;
    }

    @NonNull
    public List<Punchline> getCurrentPunchlines() {
        return currentPunchlines;
    }

    public boolean isFinished() {
        return getCurrentCheckedPunchlines() == PUNCHLINES_BY_GAME_NB;
    }

    private long getCurrentCheckedPunchlines() {
        return Stream.of(currentPunchlines).filter(Punchline::isChecked).count();
    }

    private void resetPunchlines() {
        for (Punchline punchline : currentPunchlines) {
            punchline.setChecked(false);
        }
    }
}
