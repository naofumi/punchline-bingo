/*
 * Copyright © 2017.
 * This file is part of Punchline Bingo.
 *
 * Punchline Bingo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Punchline Bingo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Punchline Bingo.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.pierreduchemin.punchlinebingo.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ScrollView;

import com.pierreduchemin.punchlinebingo.BuildConfig;
import com.pierreduchemin.punchlinebingo.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.aboutpage.AboutPage;
import mehdi.sakout.aboutpage.Element;

public class AboutActivity extends AppCompatActivity {

    @BindView(R.id.svAbout)
    ScrollView svAbout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_about);
        ButterKnife.bind(this);

        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }

        loadAbout();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }

    private void loadAbout() {

        Element versionElement = new Element()
                .setTitle(getString(R.string.info_label_version, BuildConfig.VERSION_NAME))
                .setColor(R.color.colorGrey)
                .setGravity(Gravity.CENTER_HORIZONTAL);

        Uri gitlabAddress = Uri.parse("https://gitlab.com/pierreduchemin/pushline-bingo");
        Element gitlabElement = new Element()
                .setTitle(getString(R.string.info_label_gitlab))
                .setAutoIconColor(false)
                .setIcon(R.drawable.vr_gitlab)
                .setIntent(new Intent(Intent.ACTION_VIEW, gitlabAddress));

        Element licensesElement = new Element()
                .setTitle(getString(R.string.info_label_licenses))
                .setAutoIconColor(false)
                .setIcon(R.drawable.vr_information)
                .setOnClickListener(v -> startActivity(new Intent(AboutActivity.this, LegalActivity.class)));

        View aboutPage = new AboutPage(this)
                .isRTL(false)
                .setImage(R.mipmap.ic_launcher)
                .setDescription(getString(R.string.info_label_app_description))
                .addItem(versionElement)
                .addItem(gitlabElement)
                .addItem(licensesElement)
                .create();

        svAbout.addView(aboutPage);
    }
}
