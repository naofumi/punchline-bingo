/*
 * Copyright © 2017.
 * This file is part of Punchline Bingo.
 *
 * Punchline Bingo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Punchline Bingo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Punchline Bingo.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.pierreduchemin.punchlinebingo.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.pierreduchemin.punchlinebingo.R;

import net.yslibrary.licenseadapter.LicenseAdapter;
import net.yslibrary.licenseadapter.LicenseEntry;
import net.yslibrary.licenseadapter.Licenses;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LegalActivity extends AppCompatActivity {

    @BindView(R.id.rvLicenses)
    RecyclerView rvLicenses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_legal);
        ButterKnife.bind(this);

        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }

        loadLicenses();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }

    private void loadLicenses() {

        // create list of licenses
        List<LicenseEntry> dataset = new ArrayList<>();

        dataset.add(Licenses.noContent("Bingo Icon", "gnokii", "https://openclipart.org/detail/171717/bingo"));
        dataset.add(Licenses.noContent("Trophy Icon", "netalloy", "https://openclipart.org/detail/120343/trophy"));

        dataset.add(Licenses.fromGitHub("medyo/android-about-page", Licenses.NAME_MIT));
        dataset.add(Licenses.noContent("Android SDK", "Google Inc.", "https://developer.android.com/sdk/terms.html"));
        dataset.add(Licenses.fromGitHub("JakeWharton/butterknife", Licenses.LICENSE_APACHE_V2));
        dataset.add(Licenses.fromGitHub("google/gson", Licenses.LICENSE_APACHE_V2));
        dataset.add(Licenses.fromGitHub("yshrsmz/LicenseAdapter", Licenses.LICENSE_APACHE_V2));
        dataset.add(Licenses.fromGitHub("plattysoft/Leonids", Licenses.LICENSE_APACHE_V2));
        dataset.add(Licenses.fromGitHub("florent37/ViewAnimator", Licenses.LICENSE_APACHE_V2));

        // create adapter
        LicenseAdapter adapter = new LicenseAdapter(dataset);
        rvLicenses.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvLicenses.setAdapter(adapter);

        // finally load license text from Web
        Licenses.load(dataset);
    }
}
