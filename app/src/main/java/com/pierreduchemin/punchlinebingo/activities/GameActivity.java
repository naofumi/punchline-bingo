/*
 * Copyright © 2017.
 * This file is part of Punchline Bingo.
 *
 * Punchline Bingo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Punchline Bingo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Punchline Bingo.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.pierreduchemin.punchlinebingo.activities;

import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.florent37.viewanimator.ViewAnimator;
import com.pierreduchemin.punchlinebingo.PunchlineBingo;
import com.pierreduchemin.punchlinebingo.R;
import com.pierreduchemin.punchlinebingo.adapters.PunchlineSquareAdapter;
import com.plattysoft.leonids.ParticleSystem;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GameActivity extends AppCompatActivity {

    long startTimestamp;

    @BindView(R.id.rvGame)
    RecyclerView rvGame;
    @BindView(R.id.tvVictory)
    TextView tvVictory;
    @BindView(R.id.llVictory)
    LinearLayout llVictory;
    @BindView(R.id.tvTime)
    TextView tvTime;
    @BindView(R.id.btnClose)
    Button btnClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_game);
        ButterKnife.bind(this);

        int gameSetId = getIntent().getIntExtra(PunchlineBingo.EXTRA_GAME_SET_ID, 0);

        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setTitle(PunchlineBingo.getPunchlinesSets().get(gameSetId).getName());
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }

        rvGame.setAdapter(new PunchlineSquareAdapter(GameActivity.this::showVictoryBanner));
        rvGame.setLayoutManager(new GridLayoutManager(this, 3));
        rvGame.addItemDecoration(new DividerItemDecoration(rvGame.getContext(), DividerItemDecoration.HORIZONTAL));
        rvGame.addItemDecoration(new DividerItemDecoration(rvGame.getContext(), DividerItemDecoration.VERTICAL));
        rvGame.setHasFixedSize(true);

        startTimestamp = System.currentTimeMillis() / 1000;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        if (PunchlineBingo.getGame().isFinished()) {
            super.onBackPressed();
        } else {
            new AlertDialog.Builder(GameActivity.this)
                    .setTitle(getString(R.string.info_title_exit_game))
                    .setMessage(getString(R.string.info_message_exit_game))
                    .setPositiveButton(android.R.string.yes, (dialog, which) -> GameActivity.super.onBackPressed())
                    .setNegativeButton(android.R.string.no, null)
                    .setIcon(R.drawable.vr_alert)
                    .show();
        }
    }

    public void showVictoryBanner() {
        rvGame.setClickable(false);

        new ParticleSystem(this, 40, R.drawable.animated_confetti1, 100000)
                .setSpeedModuleAndAngleRange(0f, 0.5f, 0, 0)
                .setRotationSpeed(144)
                .setAcceleration(0.00006f, 90)
                .emit(0, 0, 10);

        new ParticleSystem(this, 40, R.drawable.animated_confetti2, 100000)
                .setSpeedModuleAndAngleRange(0f, 0.5f, 0, 0)
                .setRotationSpeed(144)
                .setAcceleration(0.00006f, 90)
                .emit(0, 0, 10);

        new ParticleSystem(this, 40, R.drawable.animated_confetti3, 100000)
                .setSpeedModuleAndAngleRange(0f, 0.5f, 0, 0)
                .setRotationSpeed(144)
                .setAcceleration(0.00006f, 90)
                .emit(0, 0, 10);

        // Victory view handling
        llVictory.setVisibility(View.VISIBLE);

        long diffTimestamp = (System.currentTimeMillis() / 1000) - startTimestamp;
        tvTime.setText(DateUtils.formatElapsedTime(diffTimestamp));

        ViewAnimator.animate(llVictory)
                .fadeIn()
                .dp().translationY(-600, 0)
                .descelerate()
                .duration(1200)
                .start();

        Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(200);
    }

    @OnClick(R.id.btnClose)
    public void onClickClose() {
        onBackPressed();
    }
}
