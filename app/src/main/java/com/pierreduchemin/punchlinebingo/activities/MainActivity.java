/*
 * Copyright © 2017.
 * This file is part of Punchline Bingo.
 *
 * Punchline Bingo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Punchline Bingo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Punchline Bingo.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.pierreduchemin.punchlinebingo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.pierreduchemin.punchlinebingo.PunchlineBingo;
import com.pierreduchemin.punchlinebingo.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.btnGame)
    Button btnGame;
    @BindView(R.id.btnAbout)
    Button btnAbout;
    @BindView(R.id.btnExit)
    Button btnExit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        btnGame.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(this, R.drawable.vr_flag_checkered), null, null);
        btnAbout.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(this, R.drawable.vr_information), null, null);
        btnExit.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(this, R.drawable.vr_logout_variant), null, null);
    }

    @OnClick(R.id.btnGame)
    public void onClickLaunch(View view) {
        int punchlinesSetNb = PunchlineBingo.getPunchlinesSets().size();
        if (punchlinesSetNb <= 0) {
            Toast.makeText(this, R.string.error_message_please_add_12_punchlines_first, Toast.LENGTH_SHORT).show();
            return;
        }

        if (punchlinesSetNb == 1) {
            if (PunchlineBingo.getPunchlinesSets().get(0).isPlayable()) {
                Intent intent = new Intent(this, GameActivity.class);
                intent.putExtra(PunchlineBingo.EXTRA_GAME_SET_ID, 0);
                PunchlineBingo.getGame().generateRandomPunchlines(PunchlineBingo.getPunchlinesSets(), 0);
                startActivity(intent);
            } else {
                Toast.makeText(this, R.string.error_message_please_add_12_punchlines_first, Toast.LENGTH_SHORT).show();
            }
            return;
        }

        startActivity(new Intent(this, PunchlinesSetSelectorActivity.class));
    }

    @OnClick(R.id.btnAbout)
    public void onClickAbout(View view) {
        startActivity(new Intent(this, AboutActivity.class));
    }

    @OnClick(R.id.btnExit)
    public void onClickExit(View view) {
        finish();
    }
}
