/*
 * Copyright © 2017.
 * This file is part of Punchline Bingo.
 *
 * Punchline Bingo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Punchline Bingo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Punchline Bingo.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.pierreduchemin.punchlinebingo.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.pierreduchemin.punchlinebingo.PunchlineBingo;
import com.pierreduchemin.punchlinebingo.R;
import com.pierreduchemin.punchlinebingo.adapters.PunchlinesSetAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PunchlinesSetSelectorActivity extends AppCompatActivity {

    @BindView(R.id.rvPunchlinesSetSelector)
    RecyclerView rvPunchlinesSetSelector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_punchlines_set_selector);
        ButterKnife.bind(this);

        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }

        rvPunchlinesSetSelector.setHasFixedSize(true);
        rvPunchlinesSetSelector.addItemDecoration(new DividerItemDecoration(rvPunchlinesSetSelector.getContext(), DividerItemDecoration.VERTICAL));
        rvPunchlinesSetSelector.setLayoutManager(new LinearLayoutManager(this));
        rvPunchlinesSetSelector.setAdapter(new PunchlinesSetAdapter(PunchlineBingo.getPunchlinesSets()));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }
}
