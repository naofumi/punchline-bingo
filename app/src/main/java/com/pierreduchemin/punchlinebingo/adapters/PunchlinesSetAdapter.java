/*
 * Copyright © 2017.
 * This file is part of Punchline Bingo.
 *
 * Punchline Bingo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Punchline Bingo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Punchline Bingo.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.pierreduchemin.punchlinebingo.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.pierreduchemin.punchlinebingo.PunchlineBingo;
import com.pierreduchemin.punchlinebingo.R;
import com.pierreduchemin.punchlinebingo.activities.GameActivity;
import com.pierreduchemin.punchlinebingo.models.PunchlinesSet;

import java.util.List;

public class PunchlinesSetAdapter extends RecyclerView.Adapter<PunchlinesSetAdapter.ViewHolder> {

    private List<PunchlinesSet> punchlinesSets;
    private Context context;

    public PunchlinesSetAdapter(List<PunchlinesSet> punchlinesSets) {
        this.punchlinesSets = punchlinesSets;
    }

    @Override
    public PunchlinesSetAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.view_punchlines_set_item, parent, false);
        // set the view's size, margins, paddings and layout parameters
        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.textView.setText(punchlinesSets.get(position).getName());
        holder.textView.setOnClickListener(v -> {
            if (PunchlineBingo.getPunchlinesSets().get(holder.getAdapterPosition()).isPlayable()) {
                Intent intent = new Intent(context, GameActivity.class);
                intent.putExtra(PunchlineBingo.EXTRA_GAME_SET_ID, holder.getAdapterPosition());
                PunchlineBingo.getGame().generateRandomPunchlines(PunchlineBingo.getPunchlinesSets(), holder.getAdapterPosition());
                context.startActivity(intent);
            } else {
                Toast.makeText(context, R.string.error_message_please_add_12_punchlines_first, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return punchlinesSets.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView;

        ViewHolder(View view) {
            super(view);
            this.textView = (TextView) view.findViewById(R.id.tvPunchlinesSet);
        }
    }
}