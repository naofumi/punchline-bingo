/*
 * Copyright © 2017.
 * This file is part of Punchline Bingo.
 *
 * Punchline Bingo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Punchline Bingo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Punchline Bingo.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.pierreduchemin.punchlinebingo.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pierreduchemin.punchlinebingo.PunchlineBingo;
import com.pierreduchemin.punchlinebingo.R;
import com.pierreduchemin.punchlinebingo.models.Punchline;

public class PunchlineSquareAdapter extends RecyclerView.Adapter<PunchlineSquareAdapter.PunchlineSquareViewHolder> {

    private Context context;
    private PunchlineAction punchlineAction;

    public PunchlineSquareAdapter(@NonNull PunchlineAction punchlineAction) {
        this.punchlineAction = punchlineAction;
    }

    @Override
    public PunchlineSquareViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.view_square_punchline, parent, false);
        return new PunchlineSquareViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PunchlineSquareViewHolder holder, int position) {
        Punchline punchline = PunchlineBingo.getGame().getCurrentPunchlines().get(position);
        holder.tvPunchlineLabel.setText(punchline.getLabel());
        holder.tvPunchlineLabel.setOnClickListener(v -> {
            if (!PunchlineBingo.getGame().isFinished()) {
                Punchline currentPunchline = PunchlineBingo.getGame().getCurrentPunchlines().get(holder.getAdapterPosition());
                currentPunchline.setChecked(!currentPunchline.isChecked());

                holder.update(currentPunchline);

                if (PunchlineBingo.getGame().isFinished()) {
                    punchlineAction.victory();
                }
            }
        });

        holder.update(punchline);
    }

    @Override
    public int getItemCount() {
        return PunchlineBingo.getGame().getCurrentPunchlines().size();
    }

    public interface PunchlineAction {
        void victory();
    }

    class PunchlineSquareViewHolder extends RecyclerView.ViewHolder {
        TextView tvPunchlineLabel;
        ImageView ivPunchlineCheck;

        PunchlineSquareViewHolder(View itemView) {
            super(itemView);
            tvPunchlineLabel = (TextView) itemView.findViewById(R.id.tvPunchlineLabel);
            ivPunchlineCheck = (ImageView) itemView.findViewById(R.id.ivPunchlineCheck);
        }

        void update(@NonNull Punchline punchline) {
            ivPunchlineCheck.setVisibility(punchline.isChecked() ? View.VISIBLE : View.GONE);
            tvPunchlineLabel.setTextColor(ContextCompat.getColor(context, punchline.isChecked() ? R.color.colorGrey : android.R.color.black));
        }
    }
}
