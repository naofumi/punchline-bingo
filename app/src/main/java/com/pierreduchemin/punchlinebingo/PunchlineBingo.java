/*
 * Copyright © 2017.
 * This file is part of Punchline Bingo.
 *
 * Punchline Bingo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Punchline Bingo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Punchline Bingo.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.pierreduchemin.punchlinebingo;

import android.app.Application;
import android.support.annotation.NonNull;
import android.util.Log;

import com.pierreduchemin.punchlinebingo.models.Game;
import com.pierreduchemin.punchlinebingo.models.PunchlinesSet;
import com.pierreduchemin.punchlinebingo.utils.FileSystemUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PunchlineBingo extends Application {

    public static final String EXTRA_GAME_SET_ID = "extra_game_set_id";
    private static final String TAG = PunchlineBingo.class.getSimpleName();
    private static Game game = new Game();
    private static List<PunchlinesSet> punchlinesSets = new ArrayList<>();

    @NonNull
    public static Game getGame() {
        return game;
    }

    @NonNull
    public static List<PunchlinesSet> getPunchlinesSets() {
        return punchlinesSets;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        try {
            punchlinesSets.addAll(FileSystemUtils.getPunchlinesFromJson(this, R.raw.punchlines_fr));
        } catch (IOException e) {
            Log.e(TAG, "Cannot load punchlines from json file");
        }
    }
}
